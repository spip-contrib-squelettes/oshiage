/********************************
 *  GUI: interface utilisateur
 *
 ********************************/
 

$(document).ready(function(){

	// Lien en spip_out s'ouvre ds une nouvelle fenetre
	$('a.spip_out,a[rel*=external]').attr('target','_blank').attr('rel','external noopener noreferrer');

	// Go to the top
	// http://www.jqueryscript.net/other/Minimal-Back-To-Top-Functionality-with-jQuery-CSS3.html
	$(window).scroll(function(event){
		var scroll = $(window).scrollTop();
		if (scroll >= 800) {
			$(".go-top").addClass("show");
		} else {
			$(".go-top").removeClass("show");
		}
	});

	$('.go-top a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 1000);
	});

	// menu dock
	$('.button-burger-trigger').click(function() {
		$(".menu-panel").addClass('unfold');
		return false;
	});
	$('.button-burger-untrigger').click(function() {
		$(".menu-panel").removeClass('unfold');
		return false;
	});

	// barre recherche
	$('.header-search-trigger').click(function() {
		$(".header-search").addClass('unfold');
		return false;
	});
	$('.header-search-untrigger').click(function() {
		$(".header-search").removeClass('unfold');
		return false;
	});


	// scroll doux
	$('.smooth-scroll').click (function() {
		var target = $(this).attr('href');
		var hash = "#" + target.substring(target.indexOf('#')+1);
		$('html, body').animate({ scrollTop:$(hash).offset().top 	}, 600);
		return false;
	});


});


// WOW: animation in scroll
// https://wowjs.uk/docs.html
new WOW().init();


