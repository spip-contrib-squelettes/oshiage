<?php
/**
 * Chargement du plugin Oshiage
 *
 * @plugin     Oshiage
 * @copyright  2022
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Oshiage\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// définir la couleur du navigateur mobile
define('_FAVICON_COULEUR', '6fa12f');

// pagination : 6 liens max
define('_PAGINATION_NOMBRE_LIENS_MAX', 7);