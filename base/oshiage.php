<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function oshiage_declarer_champs_extras($champs = array()) {

	// Table spip_rubriques
	$champs['spip_rubriques']['oshiage_type_rubrique'] = array(
		'saisie' => 'radio',
		'options' => array(
			'nom' => 'oshiage_type_rubrique',
			'label' => _T('oshiage:oshiage_type_rubrique'),
			'sql' => "varchar(30) NOT NULL DEFAULT ''",
			'defaut' => 'tri_date',
			'data' => array(
				'tri_date' => _T('oshiage:oshiage_type_rubrique_tri_date'),
				'tri_date_archive' => _T('oshiage:oshiage_type_rubrique_tri_date_archive'),
 				'tri_num' => _T('oshiage:oshiage_type_rubrique_tri_num'),
			),
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_rubriques']['surtitre'] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'surtitre',
			'label' => _T('oshiage:oshiage_rubrique_surtitre'),
			'explication' => _T('oshiage:oshiage_rubrique_surtitre_explication'),
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'traitements' => '_TRAITEMENT_TYPO',
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	$champs['spip_rubriques']['titre_long'] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'titre_long',
			'label' => _T('oshiage:oshiage_rubrique_titre_long'),
			'explication' => _T('oshiage:oshiage_rubrique_titre_long_explication'),
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'traitements' => '_TRAITEMENT_TYPO',
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	// Table spip_selections_contenus
	$champs['spip_selections_contenus']['surtitre'] = array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'surtitre',
			'label' => _T('oshiage:oshiage_rubrique_surtitre'),
			'explication' => _T('oshiage:oshiage_rubrique_surtitre_explication'),
			'sql' => "varchar(255) NOT NULL DEFAULT ''",
			'defaut' => '',// Valeur par défaut
			'traitements' => '_TRAITEMENT_TYPO',
			'restrictions' => array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => 'webmestre')),	//Seuls les webmestres peuvent modifier
		),
	);

	// Table spip_articles
	$champs['spip_articles']['cacher_a2a'] = array(
		'saisie' => 'case',
		'options' => array(
			'nom' => 'cacher_a2a',
			'label_case' => _T('oshiage:cacher_a2a'),
			'sql' => "varchar(3) NOT NULL DEFAULT ''",
			'defaut' => '',
			'restrictions'=>array('voir' => array('auteur' => ''),	//Tout le monde peut voir
							'modifier' => array('auteur' => '')),	//Tout le monde peuvent modifier
		),
	);

	return $champs;
}
