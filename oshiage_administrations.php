<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin oshiage
 *
 * @plugin     oshiage
 * @copyright  2022-2023
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\oshiage\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/meta');
include_spip('inc/cextras');
include_spip('base/oshiage');


/**
 * Fonction d'installation et de mise à jour du plugin oshiage.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function oshiage_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(oshiage_declarer_champs_extras(), $maj['create']);
	cextras_api_upgrade(oshiage_declarer_champs_extras(), $maj['1.0.1']);
	cextras_api_upgrade(oshiage_declarer_champs_extras(), $maj['1.0.2']);
	cextras_api_upgrade(oshiage_declarer_champs_extras(), $maj['1.0.3']);
	cextras_api_upgrade(oshiage_declarer_champs_extras(), $maj['1.0.4']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin oshiage
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function oshiage_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(oshiage_declarer_champs_extras());
	effacer_config('oshiage');
	effacer_meta($nom_meta_base_version);
}
